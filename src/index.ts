import mongoose = require('mongoose');
import App from './server/app';
import { environment } from './environments/environment';
import log4js = require('log4js');
var logger = log4js.getLogger('index.ts');
logger.level = 'all';


mongoose.Promise = global.Promise;
mongoose.connect( String( environment.MONGO_URL ),{useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true} )
        .then(() => {
            logger.info('=========== Database connection established successfully ===========');

            //Server Creation
            const app = App.init(  environment.PORT );
            app.start( () => {
                logger.info(`Server running correctly in url: ${environment.HOST}`);
            });
        })
        .catch((err: any) => logger.error(err));