import express = require('express');
import controllerComment from '../controllers/comment.controller';
import controllerVotingRoom from '../controllers/votingRoom.controller';
import controllerVote from '../controllers/vote.controller';

export default class Routes {
    private app: express.Application;

    constructor(){
        this.app = express();
        this.loadRoutes();
    }

    private loadRoutes(): void {
        this.app.use( controllerComment );
        this.app.use( controllerVotingRoom );
        this.app.use( controllerVote );
    }

    public getRoutes(): any {
        return this.app;
    }
}