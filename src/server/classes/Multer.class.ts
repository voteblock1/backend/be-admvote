import multer from 'multer';
import { v4 as uuidv4 } from 'uuid';
import path from 'path';

export default class Multer {

    private pathImage: string;
    constructor(){
        this.pathImage = ''; 
    }

    /**
     * @description This method save images.
     * @date 14/10/2020
     * @author scespedesg
     * @param {*} message
     */
    public saveImageServer(message: string): any {
        if (message === 'company') {
            this.pathImage = 'src/public/uploads/company/image/';
        } else if (message === 'user') {
            this.pathImage = 'src/public/uploads/user/image/';
        } else if (message === 'room') {
            this.pathImage = 'src/public/uploads/room/image/';
        } else if (message === 'candidate') {
            this.pathImage = 'src/public/uploads/candidate/image/';
        }

        const storage = multer.diskStorage({
                destination: this.pathImage,
                filename: ( req, file, cb ) => {
                    cb(null, uuidv4() + path.extname(file.originalname));
                }
            });

        return multer({storage});
    }
}

// const storage = multer.diskStorage({
//     destination: 'src/public/uploads/company/image/',
//     filename: ( req, file, cb ) => {
//         cb(null, uuidv4() + path.extname(file.originalname));
//     }
// });

// export default multer({storage});