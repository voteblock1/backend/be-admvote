export default class VoteService {

    public contract: any;

    constructor(contract: any) {
        this.contract = contract;
    }

    public async saveVote(id: string, idCandidate: string,
            idRoom: string, createdAt: string, from: any): Promise<any> {
        return await this.contract.saveVote(id, idCandidate, idRoom, createdAt, {from, gas: '1000000'});
    }

    public async getTotalVote(): Promise<any> {
        return Number(await this.contract.totalVote());
    }

    public async getVotes(): Promise<any>{
        let total = await this.getTotalVote();
        let votes = [];

        for(let i = 0; i < total; i++) {
            let vote = await this.contract.votes(i);
            votes.push(vote);
        }
        
        return votes.map((e) => {
            return {
                id: e[0],
                idCandidate: e[1],
                idRoom: e[2],
                createdAt: e[3]
            }
        });
    }
}