let VotesContract = require('../../../output/VotesContract.json');
let contract = require("truffle-contract");

export default async (provider: any) => {
    const votes = contract(VotesContract);
    votes.setProvider(provider);

    votes.deployed();
    let instance = await votes.deployed();
    return instance;
}