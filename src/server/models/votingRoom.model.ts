import { Schema, Document, model } from 'mongoose';

let votingRoomSchema = new Schema({
    idVoter: {
        type: String,
        required: [true, 'The idVoter is necessary'],
    },
    idRoom: {
        type: String,
        required: [true, 'The idRoom is necessary'],
    },
    state: {
        type: Boolean,
        default: true
    }
},
{
    timestamps: true
});


interface IVotingRoom extends Document {
    idVoter: string;
    idRoom: string;
    state: boolean;
}

export default model<IVotingRoom>('VotingRoom', votingRoomSchema);