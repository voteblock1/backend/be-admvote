import { Schema, Document, model } from 'mongoose';

let voteSchema = new Schema({
    idVoter: {
        type: String,
        required: [true, 'The idVoter is necessary'],
    },
    idRoom: {
        type: String,
        required: [true, 'The idRoom is necessary'],
    },
    idCandidate: {
        type: String,
        required: [true, 'The idCandidate is necessary'],
    },
    createdAt: {
        type: Date,
        default: new Date(),
    },
});


interface IVote extends Document {
    idVoter: string;
    idRoom: string;
    idCandidate: string;
}

export default model<IVote>('Vote', voteSchema);