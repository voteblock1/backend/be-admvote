import { Schema, Document, model } from 'mongoose';

let commentSchema = new Schema({
    idVoter: {
        type: String,
        required: [true, 'The idVoter is necessary'],
    },
    idRoom: {
        type: String,
        required: [true, 'The idRoom is necessary'],
    },
    state: {
        type: Boolean,
        default: true
    },
    comment: {
        type: String,
        required: [true, 'The comment is necessary'],
    },
},
{
    timestamps: true
});


interface IComment extends Document {
    idVoter: string;
    idRoom: string;
    state: boolean;
    comment: string;
}

export default model<IComment>('Comment', commentSchema);