import express = require('express');
import Global from './global.middleware';
import Authentication from './authentication.middleware';

export default  class Middlewares {
    public app: express.Application;
    public global: Global;
    public authentication: Authentication;

    constructor() {
        this.app = express();
        this.global = new Global();
        this.authentication = new Authentication();
        this.loadMiddlewaresGlobal();
    }

    /**
     * @description Method for load the middlewares global.
     * @date 07/10/2020
     * @author scespedes
     * @version 1.0.0
     */
    private loadMiddlewaresGlobal(): void {
        this.app.use( this.global.getApp() );
    }

    /**
     * @description Return the settings for Middleware to settings principal index.
     * @date 07/10/2020
     * @author scespedes
     * @version 1.0.0
     */
    public  getMiddleware(): any {
        return this.app;
    }

    /**
     * @description Return the settings for MiddlewareAuthentication.
     * @date 07/10/2020
     * @author scespedes
     * @version 1.0.0
     */
    public getMiddlewareAuthentication(): any {
        return this.authentication;
    }
}