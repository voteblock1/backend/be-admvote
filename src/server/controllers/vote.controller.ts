import { Router, Request, Response } from 'express';
import _ = require('underscore');
import Authentication from '../middlewares/authentication.middleware';
import Vote from '../models/vote.model';
const CVote = Router();
const authentication = new Authentication();
const Web3 = require('web3');
import VoteContract from '../classes/vote';
import VoteService from '../classes/voteServices';
import { environment } from '../../environments/environment';
const provider = new Web3.providers.HttpProvider(environment.GANACHE);
const web3 = new Web3(provider);


/**
 * @description Method for creating vote.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVote.post( '/vote', [ authentication.buildCheckToken() ], ( req: Request, res: Response ) => {
    let body = req.body;

    let vote = new Vote({
        idVoter: body.idVoter,
        idRoom: body.idRoom,
        idCandidate: body.idCandidate,
    });
    vote.save((err, vote:any) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        VoteContract(web3.currentProvider).then(async (e: any) => {
            const voteServices = new VoteService(e);
            web3.eth.getAccounts(async (error: any, result: any) => {
                if(error) {
                    return res.status(500).json({
                        ok: false,
                        error
                    });
                } else {
                    console.log(await voteServices.getTotalVote());
                    voteServices.saveVote(vote._id.toString(), vote.idCandidate.toString(), 
                        vote.idRoom.toString(), vote.createdAt.toString(), result[0].toString())
                        .then((result) => {
                            res.json({
                                ok: true,
                                vote: vote,
                                transaction: result
                            });
                        })
                        .catch( err => {
                            return res.status(500).json({
                                ok: false,
                                err
                            });
                        });
                }
            });
        }).catch(err => {
            return res.status(500).json({
                ok: false,
                err
            });
        });
    });
});


/**
 * @description Method to get all vote.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVote.get('/vote', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 100;
    limit = Number(limit);

    Vote.find()
            .skip(from)
            .limit(limit)
            .exec((err, vote) => {
                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                Vote.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        vote,
                        howMany: count
                    });
                });
            });
});

/**
 * @description Method to get vote by id.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVote.get('/vote/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;
    if(id == null)
    {
        return res.status(404).json({message: "Error vote doesn't exist"});
    }

    Vote.findById(id, (err, vote) => {

            if(err) {return res.status(500).json({message: 'Error returning data'});}

            if(! vote) return res.status(404).json({message: "Error vote doesn't exist"});

            return res.status(200).json({
                ok: true,
                vote
            });
        });
});


/**
 * @description Método para validar si ya voto en una sala.
 * @date 04/11/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVote.get('/vote/checkVote/:idUser/:idRoom', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let idUser = req.params.idUser;
    let idRoom = req.params.idRoom;
    if(idUser == null || idRoom == null)
    {
        return res.status(404).json({message: "Error vote doesn't exist"});
    }

    Vote.find({idVoter: idUser, idRoom: idRoom}, (err, vote) => {

            if(err) {return res.status(500).json({message: 'Error returning data'});}

            if(! vote) return res.status(404).json({ok: false, message: "Error vote doesn't exist"});

            return res.status(200).json({
                ok: true,
                vote
            });
        });
});


/**
 * @description Method to get result vote.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVote.get('/vote/result/:idRoom', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let idRoom = req.params.idRoom;

    Vote.aggregate([
        {
            $match: {idRoom: idRoom}
        },
        {
            $group: {_id: '$idCandidate', totalVotes:{$sum: 1}}
        }
    ])
    .exec((err, vote) => {

        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }
        
        res.status(200).json({
            ok: true,
            vote
        });
    });
});
export default CVote;