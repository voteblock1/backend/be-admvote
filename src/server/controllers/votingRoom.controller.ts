import { Router, Request, Response } from 'express';
import _ = require('underscore');
import Authentication from '../middlewares/authentication.middleware';
import VotingRoom from '../models/votingRoom.model';
const CVotingRoom = Router();
const authentication = new Authentication();


/**
 * @description Method for creating votingRoom.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVotingRoom.post( '/votingRoom', [ authentication.buildCheckToken() ], ( req: Request, res: Response ) => {
    let body = req.body;

    let votingRoom = new VotingRoom({
        idVoter: body.idVoter,
        idRoom: body.idRoom,
    });
    votingRoom.save((err, votingRoom) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            typeToken: votingRoom
        });
    });
});


/**
 * @description Method to get all votingRoom.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVotingRoom.get('/votingRoom', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 15;
    limit = Number(limit);

    VotingRoom.find({state: true})
            .skip(from)
            .limit(limit)
            .exec((err, votingRoom) => {
                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                VotingRoom.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        votingRoom,
                        howMany: count
                    });
                });
            });
});

/**
 * @description Method to get votingRoom by id.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVotingRoom.get('/votingRoom/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;
    if(id == null)
    {
        return res.status(404).json({message: "Error votingRoom doesn't exist"});
    }

    VotingRoom.findById(id, (err, votingRoom) => {

            if(err) {return res.status(500).json({message: 'Error returning data'});}

            if(! votingRoom) return res.status(404).json({message: "Error votingRoom doesn't exist"});

            return res.status(200).json({
                ok: true,
                votingRoom
            });
        });
});


/**
 * @description Método para validar si ya pertenece a una sala.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVotingRoom.get('/votingRoom/checkUser/:idUser/:idRoom', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let idUser = req.params.idUser;
    let idRoom = req.params.idRoom;
    if(idUser == null || idRoom == null)
    {
        return res.status(404).json({message: "Error votingRoom doesn't exist"});
    }

    VotingRoom.find({idVoter: idUser, idRoom: idRoom}, (err, votingRoom) => {

            if(err) {return res.status(500).json({message: 'Error returning data'});}

            if(! votingRoom) return res.status(404).json({ok: false, message: "Error votingRoom doesn't exist"});

            return res.status(200).json({
                ok: true,
                votingRoom
            });
        });
});



/**
 * @description Method that delete a votingRoom.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVotingRoom.delete('/votingRoom/:id', [ authentication.buildCheckToken() ], function ( req: Request, res: Response ) {
    let id = req.params.id;

    let changeState = {
        state: false
    };
    VotingRoom.findByIdAndUpdate( id, changeState, {new: true}, (err, votingRoom) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(votingRoom === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'votingRoom not found'
                    }
                });
            }
            res.json({
                ok: true,
                user: votingRoom
            });
    });
});


/**
 * @description Método todas las salas de un usuario.
 * @date 04/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVotingRoom.get('/votingRoom/myRooms/:idUser', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let idUser = req.params.idUser;
    if(idUser == null)
    {
        return res.status(404).json({message: "Error votingRoom doesn't exist"});
    }

    VotingRoom.find({idVoter: idUser}, (err, votingRoom) => {

            if(err) {return res.status(500).json({message: 'Error returning data'});}

            if(! votingRoom) return res.status(404).json({ok: false, message: "Error votingRoom doesn't exist"});

            return res.status(200).json({
                ok: true,
                votingRoom
            });
        });
});
export default CVotingRoom;