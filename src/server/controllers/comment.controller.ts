import { Router, Request, Response } from 'express';
import _ = require('underscore');
import Authentication from '../middlewares/authentication.middleware';
import Comment from '../models/comment.model';
const CComment = Router();
const authentication = new Authentication();


/**
 * @description Method for creating comment.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CComment.post( '/comment', [ authentication.buildCheckToken() ], ( req: Request, res: Response ) => {
    let body = req.body;

    let comment = new Comment({
        idVoter: body.idVoter,
        idRoom: body.idRoom,
        comment: body.comment,
    });
    comment.save((err, comment) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            comment
        });
    });
});


/**
 * @description Method to get all comment.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CComment.get('/comment', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 15;
    limit = Number(limit);

    Comment.find({state: true})
            .skip(from)
            .limit(limit)
            .exec((err, comment) => {
                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                Comment.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        comment,
                        howMany: count
                    });
                });
            });
});

/**
 * @description Method to get comment by id.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CComment.get('/comment/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;
    if(id == null)
    {
        return res.status(404).json({message: "Error comment doesn't exist"});
    }

    Comment.findById(id, (err, comment) => {

            if(err) {return res.status(500).json({message: 'Error returning data'});}

            if(! comment) return res.status(404).json({message: "Error comment doesn't exist"});

            return res.status(200).json({
                ok: true,
                comment
            });
        });
});

/**
 * @description Method that updates the attributes of a comment.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CComment.put('/comment/:id', [ authentication.buildCheckToken() ], function( req: Request, res: Response ){
    let id = req.params.id;
    let body =_.pick( req.body, [
        'comment',
    ]);

    Comment.findByIdAndUpdate( id, body, {new: true, runValidators: false}, (err, comment) => {

        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(! comment) return res.status(404).json({message: "Error comment doesn't exist"});

        res.json({
            ok: true,
            user: comment
        });
    });
});

/**
 * @description Method that delete a comment.
 * @date 27/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CComment.delete('/comment/:id', [ authentication.buildCheckToken() ], function ( req: Request, res: Response ) {
    let id = req.params.id;

    let changeState = {
        state: false
    };
    Comment.findByIdAndUpdate( id, changeState, {new: true}, (err, comment) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(comment === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'comment not found'
                    }
                });
            }
            res.json({
                ok: true,
                user: comment
            });
    });
});


export default CComment;