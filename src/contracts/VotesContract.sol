pragma solidity ^0.4.24;

contract VotesContract {
    struct Vote {
        string id;
        string idCandidate;
        string idRoom;
        string createdAt;
    }
    
    Vote[] public votes;

    function saveVote(string id, string idCandidate, string idRoom, string createdAt) public {
        votes.push(Vote(id, idCandidate, idRoom, createdAt));
    }

    function totalVote() public view returns (uint) {
        return votes.length;
    }
}