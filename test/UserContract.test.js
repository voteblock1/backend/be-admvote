const VotesContract = artifacts.require("./VotesContract.sol");

let instance;

beforeEach(async () => {
    instance = await VotesContract.new();
});

contract('The VotesContract', async () => {

    it('should save a vote', async () => {
        await instance.saveVote('8fc90d6f-8b93-49f9-9eef-7c81147fbba3', 
            '37c8e12b-6b56-43e6-82d4-7575643c5bc5', '1483c9ba-a963-48cf-a5e8-370e9a8ede03', 
            'Wed Dec 16 2020 11:03:35 GMT-0500 (hora estándar de Colombia)');
        let total = await instance.totalVote();
        assert(total > 0);
    });

    it('Check vote save', async () => {
        await instance.saveVote('ed56d45e-4829-4ec2-bd55-4c60ac1e5105', 
            'f58a0836-e984-4793-b36c-c69a936804e1', '42913fde-d1ff-42e9-9eaa-4af871f66f49', 
            'Wed Dec 16 2020 10:49:53 GMT-0500 (hora estándar de Colombia)');
        let vote = await instance.votes(0);
        assert.equal('ed56d45e-4829-4ec2-bd55-4c60ac1e5105', vote['id']);
    });
});